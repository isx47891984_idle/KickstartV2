# Ordenes que son obligatorias al hacer una instalacion  
Nota que aunque obligatorias, no es necesario ponerlas en el fichero de configuracion de Kickstart.  
Al llegar a un punto donde no encuentra una opcion obligatoria parara el proceso de instalacion para preguntar al usuario, luego continuara.
* [nfs](https://gitlab.com/isx47891984/KickstartV2/blob/master/opciones/nfs.md) #Sirve para indicar que el origen de la instalacion es un servidor nfs (Otras opciones: cdrom, harddrive, url(para ftp y http))
* [auth or authconfig](https://gitlab.com/isx47891984/KickstartV2/blob/master/opciones/auth.md)  #Sirve para definir el tipo de autenticación en el sistema
* [bootloader](https://gitlab.com/isx47891984/KickstartV2/blob/master/opciones/bootloader.md)  # Sirve para configurar el gestor de arranque del sistema (GRUB por defecto, se puede cambiar con --useLilo, si ya usamos LILO definir --upgrade para conservarlo)
* [keyboard](https://gitlab.com/isx47891984/KickstartV2/blob/master/opciones/keyboard.md)  # Sirve para configurar nuestro teclado
* [lang](https://gitlab.com/isx47891984/KickstartV2/blob/master/opciones/lang.md)  # Sirve para especificar el idioma de la instalacion
* [rootpw](https://gitlab.com/isx47891984/KickstartV2/blob/master/opciones/rootpw.md)  # Sirve para especificar la contraseña de root
* [timezone](https://gitlab.com/isx47891984/KickstartV2/blob/master/opciones/timezone.md)  # Sirve para configurar la zona horaria del sistema

# Ordenes que no son obligatorias al hacer una instalacion pero que usamos
* install # Sirve para indicar que vamos a realizar una instalacion (Contraparte: upgrade)
    + Es opcional porque es elegida por defecto, es obligatorio indicar el origen de la instalacion
* [firewall](https://gitlab.com/isx47891984/KickstartV2/blob/master/opciones/firewall.md) # Sirve para activar o desactivar el firewall, si lo activamos permite añadir excepciones (servicios, puertos, dispositivos)
* [clearpart](https://gitlab.com/isx47891984/KickstartV2/blob/master/opciones/clearpart.md) # Sirve para eliminar particiones de un disco duro
* [network](https://gitlab.com/isx47891984/KickstartV2/blob/master/opciones/network.md) # Sirve para configurar la red en nuestro equipo
* reboot # Sirve para indicar que debe reinicar el equipo al finalizar la instalacion  
* text # Sirve para indicar que la instalacion se realizara en modo texto (Modo grafico por defecto)  
* [xconfig](https://gitlab.com/isx47891984/KickstartV2/blob/master/opciones/xconfig.md) # Sirve para configurar el entorno gráfico X Windows (Contraparte: skipx)  
* skipx # Sirve para evitar que configure X en el equipo (Contraparte: xconfig)  
   + Si no se especifica  skipx y ni xconfig anaconda intentara configurar automaticamente el Sistema de ventanas X

# Ordenes necesarias para un fichero de configuracion de Kickstart de actualización.
    Otras ordenes serán ignoradas.  
* upgrade # Sirve para indicar que es una actualizacion
* lang  
* langsuport
* Metodo de la instalacion (nfs, ftp, http, cdrom, harddrive)
* device (si necesitas usar un device durante la actualizacion)
* keyboard 
* bootloader

Para mas opciones de Kickstart consulta la documentacion oficial.  
"[Ingles] (https://access.redhat.com/documentation/en-US/Red_Hat_Enterprise_Linux/6/html/Installation_Guide/s1-kickstart2-options.html)"  
"[Español] (http://web.mit.edu/rhel-doc/3/rhel-sag-es-3/s1-kickstart2-options.html)"