% Instalación headless con kickstart y VNC
% David Pareja Rodríguez
% 18 de Junio 2015

# Que es KickStart?
* Metodo de instalación automatizada de RedHat (portada a otras distribuciones).  
Similar a FAI(Fully Automatic Installer) de Debian o a AutoYaST de SUSE.  
Consiste en un archivo de texto plano que contiene tres secciones (orden no importa).
	1. Opciones (Configuración)
	2. Paquetes (%packages)
	3. Scripts (%pre y %post)

# Para que quiero KickStart?
* Para automaticar instalaciones repetitivas de maquinas.
	+ Granja de Servidores
	+ Laboratorio
	+ Escuela
	+ CyberCafe
	+ Oficina

# Seccion de opciones
Todas la opciones por las que el instalador preguntaria al usuario.  

# Opciones requeridas
* nfs, cdrom, harddrive o url
* auth o authconfig
* bootloader
* keyboard
* lang
* rootpw
* timezone

# Opciones no requeridas que usamos
* install
* firewall 
* clearpart
* network
* reboot 
* text
* upgrade
* xconfig
* skipx
	+ Si no se especifica  skipx y ni xconfig anaconda intentara configurar automaticamente el Sistema de ventanas X

# Opciones en el caso de hacer una actualizacion

* upgrade
* lang
* langsuport
* Metodo de la instalacion (nfs, ftp, http, cdrom, harddrive)
* device (si necesitas usar un device durante la actualizacion)
* keyboard 
* bootloader

# Seccion de Paquetes
En esta seccion indicamos los paquetes que queremos instalar en el sistema.  
La seccion empieza con %packages y acaba con %end.  
Opciones adicionales a %packages :

* --nobase
* --resolvedeps
* --ignoredeps
* --ignoremissing

Podemos especificar grupos de paquetes (@) o paquetes individuales.

# Seccion de Scripts
Indicamos comandos para ejecutar en el sistema antes y despues de la instalacion.

# Script Pre-Instalacion
Esta seccion empieza con %pre y acaba con %end.  
Se ejecuta despues de que el fichero de configuracion de kickstart se haya **analizado** y antes de que se haya **ejecutado**.  
No se ejecuta en el entorno chroot(/mnt/sysimage/).  
Sin resolucion de nombres.  
Comandos limitados.

# Script Pre-Instalacion
Opciones adicionales de %pre :

* --interpreter= 
* --erroronfail
* --log= 
    + --log=/mnt/sysimage/var/log/pre-script.log

# Script de Post-Instalacion
Esta seccion empieza con %post y acaba con %end.  
Se ejecuta despues de que haya terminado la instalacion.  
Se ejecuta en el entorno chroot(/mnt/sysimage/).  
Comandos extendidos con los paquetes instalados.

# Script de Post-Instalacion
Opciones adicionales de %post:

* --interpreter= 
* --nochroot 
* --log= 
	+ /var/log/pre-script.log (Sin opcion --nochroot)
	+ /mnt/sysimage/var/log/pre-script.log (Con opcion --nochroot)