# Script de Pre-Instalacion
Este script se ejecuta despues de que el fichero de configuracion de kickstart se haya **analizado** y antes de que se haya **ejecutado**.  
No se ejecuta en el entorno chroot(/mnt/sysimage/).  
En este punto no funciona la resolucion de nombres, por lo tanto si necesitamos acceder a la red usaremos direcciones IP.  
Esta seccion empieza con *%pre* y acaba con *%end*, a *%pre* podemos añadir las siguientes opciones.  
* --interpreter= Permite indicar cualquier lenguaje de scripting disponible en el sistema como interprete:
    + /usr/bin/sh
    + /usr/bin/bash
    + /usr/bin/python
* --erroronfail Muestra un mensaje de error y detiene la instalacion si el script falla.
* --log= Guarda un log de la ejecucion del script en el fichero especificado.
    + /mnt/sysimage/var/log/pre-script.log
    
Comandos disponibles en el entorno de pre-script:  
Extraidos de la [guia de instalacion de RHEL 6](https://access.redhat.com/documentation/en-US/Red_Hat_Enterprise_Linux/6-Beta/html/Installation_Guide/s1-redhat-config-kickstart-prescript.html)  
*arping, awk, basename, bash, bunzip2, bzcat, cat, chattr, chgrp, chmod, chown, chroot, chvt, clear, cp, cpio, cut, date, dd, df, dirname, dmesg, du, e2fsck, e2label, echo, egrep, eject, env, expr, false, fdisk, fgrep, find, fsck, fsck.ext2, fsck.ext3, ftp, grep, gunzip, gzip, hdparm, head, hostname, hwclock, ifconfig, insmod, ip, ipcalc, kill, killall, less, ln, load_policy, login, losetup, ls, lsattr, lsmod, lvm, md5sum, mkdir, mke2fs, mkfs.ext2, mkfs.ext3, mknod, mkswap, mktemp, modprobe, more, mount, mt, mv, nslookup, openvt, pidof, ping, ps, pwd, readlink, rm, rmdir, rmmod, route, rpm, sed, sh, sha1sum, sleep, sort, swapoff, swapon, sync, tail, tar, tee, telnet, top, touch, true, tune2fs, umount, uniq, vconfig, vi, wc, wget, xargs, zcat*  

# Script de Post-Instalacion
Este script se ejecuta despues de que haya terminado la instalacion, se ejecuta en el entorno chroot(/mnt/sysimage/).  
Esta seccion empieza con *%post* y acaba con *%end*, a *%post* podemos añadir las siguientes opciones.  
* --interpreter= Permite indicar cualquier lenguaje de scripting disponible en el sistema como interprete:
    + /usr/bin/sh
    + /usr/bin/bash
    + /usr/bin/python
* --nochroot Ejecuta el escript fuera del entorno de chroot.
* --log= Guarda un log de la ejecucion del script en el fichero especificado.
    + /var/log/pre-script.log (Sin opcion --nochroot)
    + /mnt/sysimage/var/log/pre-script.log (Con opcion --nochroot)
    
En este punto puedes usar comandos que hayan sido instalados por los paquetes que seleccionaste en la seccion de %packages