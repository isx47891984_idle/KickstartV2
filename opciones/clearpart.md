# clearpart

* --all

	>Elimina todas las particiones del sistema
* --drives=

	>Especifica desde cuáles unidades limpiar las particiones
* --initlabel

	>Inicializa la etiqueta del disco al valor para la arquitectura por defecto
* --linux

	>Elimina todas las particiones Linux
* --none

	>No elimina ninguna partición