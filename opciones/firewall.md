# firewall

* --enabled

	>Rechaza las conexiones entrantes que no son en respuesta a peticiones salientes, tales como respuestas DNS o peticiones DHCP
* --disabled

	>No configura ninguna regla iptables
* --trust=

	>Al listar un dispositivo aquí, tal como eth0, permite que todo el tráfico proveniente de ese dispositivo pase a través del firewall.
* --port=

	>Puede especificar que los puertos sean permitidos a a través del cortafuegos usando el formato port:protocol