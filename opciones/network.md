# network

* --bootproto=

	>Uno de dhcp, bootp, o static. Por defecto dhcp. bootp y dhcp se tratán de la misma forma. 
* --device=

	>Usado para seleccionar un dispositivo Ethernet específico para la instalación (El archivo kickstart debera ser local)
* --ip=

	>Inicializa la etiqueta del disco al valor para la arquitectura por defecto
* --gateway=

	>Elimina todas las particiones Linux
* --nameserver=

	>No elimina ninguna partición
* --nodns

	>No configura un servidor DNS
* --netmask=

	>Máscara de red para el sistema instalado
* --hostname=

	>Nombre del host para el sistema instalado