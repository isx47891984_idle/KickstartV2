# part / partition

--size=
	> El tamaño mínimo de la partición en megabytes. Especifique un valor entero aquí tal como 500. No le agregue MB al número!. 
--grow
	> Le indica a la partición que crezca para llenar el espacio disponible (si existe) o hasta el máximo tamaño. 
--maxsize=
	> El tamaño máximo de la partición en megabytes cuando la partición es configurada a crecer. Especifique un valor entero aquí y no agregue MB al número. 
--noformat
	> Le dice al programa de instalación que no formatee la partición, para usar con el comando --onpart. 
--onpart= o --usepart=
	> Coloca la partición en el dispositivo existente. Por ejemplo:
	> partition /home --onpart=hda1
	> colocará /home en /dev/hda1, el cual debe existir. 
--ondisk= o --ondrive=
	> Fuerza a la partición a que se cree en un disco particular. Por ejemplo, --ondisk=sdb colocará la partición en el segundo disco SCSI del sistema. 
--asprimary
	> Fuerza la asignación automática de la partición como una partición primaria o el particionamiento fallará. 
--fstype=
	> Coloca el tipo de sistema de archivos para la partición. Los valores válidos son ext2, ext3, swap, y vfat. 
--start=
	> Especifica el cilindro donde comienza la partición. Requiere que se especifique una unidad con --ondisk= o ondrive=. También requiere que se especifique el cilindro final con --end= o que el tamaño de la partición se especifique con --size=. 
--end=
	> Especifica el cilindro final para la partición. Requiere que el cilindro de comienzo se especifique con --start=. 
